# BitBucket Hg to Git
Ed Randall, 2/11/19

WARNING: This script DO NOT migrate repository wiki!!!

Following the recent announcement that BitBucket will be 
['sunsetting' support for Mercurial repositories from 1/6/20](https://bitbucket.org/blog/sunsetting-mercurial-support-in-bitbucket) 
it became fairly urgent for me to find a straightforward solution to migrate my Hg projects over to Git.

I only have a few projects but an automated approach should be less error-prone and able to preserve 
project metadata where possible. I therefore created this simple tool to handle my migration needs.

This project is a simple Python 3 script that makes use of the BitBucket 2.0 API to identify and 
create new equivalent Git repositories and a Git/Mercurial compatibility addon to do the conversion.

Please refer to the following resources for more information:

*  [Project wiki](https://bitbucket.org/edrandall/bitbucket-hg-to-git/wiki/)
*  [FAQ](https://bitbucket.org/edrandall/bitbucket-hg-to-git/wiki/FAQ)
*  [Issues](https://bitbucket.org/edrandall/bitbucket-hg-to-git/issues)
*  [Slack channel bbhg2git](https://bbhg2git.slack.com)

The script can be found in a Git project [bitbucket-hg-to-git](https://bitbucket.org/edrandall/bitbucket-hg-to-git) 
and is dedicated to the community. 


Please feel free to modify it for your own needs or even raise issues and pull-requests if necessary.


